//
//  Constants.swift
//  Empresas Ios
//
//  Created by ioasys on 25/12/19.
//  Copyright © 2019 danielgomes. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    
    struct Storyboards {
        static let login = "Main"
        static let enterprises = "Enterprises"
    }
}

